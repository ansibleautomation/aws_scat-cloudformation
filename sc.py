#!/bin/python
import re
import os
import sys
import boto3
import time
from string import ascii_lowercase
from string import digits
import argparse
import uuid

session = boto3.Session(profile_name='kkenduser')
client = session.client('servicecatalog')

parser = argparse.ArgumentParser()
parser.add_argument('--name','-n',help="Name of resource")
parser.add_argument('--instype','-i',help="Instance Type")
parser.add_argument('--vpc','-v',help="VPC")
parser.add_argument('--imgid','-id',help="Image Id")
parser.add_argument('--asg','-a',help="Access Security Group")
parser.add_argument('--subnet','-s',help="Subnet")


args =  parser.parse_args()
name = args.name
instype = args.instype

uid = uuid.uuid4().hex[:8]
pprodname = "DeployedProduct-"+uid

response = client.search_products()
for item in response[unicode('ProductViewSummaries')]:
	if item[unicode('Name')] == unicode(name):
		prodid = item[unicode('ProductId')]
        else:
		raise Exception()
lpaths = client.list_launch_paths(ProductId=prodid)
pathid = lpaths[unicode('LaunchPathSummaries')][0]['Id']
lstproart = client.list_provisioning_artifacts(ProductId=prodid)
proartid = lstproart[unicode('ProvisioningArtifactDetails')][0]['Id']

launchresp = client.provision_product(
    ProductId=prodid,
    ProvisioningArtifactId=proartid,
    PathId=pathid,
    ProvisionedProductName=pprodname,
    ProvisioningParameters=[
        {
            'Key': 'KeyName',
            'Value': 'ansible_testkey'
        },
        {
            'Key': 'InstanceType',
            'Value': instype
        },
        {
            'Key': 'SSHLocation',
            'Value': '10.0.0.0/24'
        }
    ],
    Tags=[ 
      { 
         'Key': 'Name',
         'Value': 'MYEC2'
      }
   ]
)
print launchresp
